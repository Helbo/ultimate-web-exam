import requests
def test_api_get_crimes():
    # Define the URL of the API endpoint
    url = "http://bottle:4000/api/get-crimes"

    # Send a GET request to the endpoint
    response = requests.get(url)

    # Check the status code of the response
    if response.status_code == 200:
        print("Success Status code is 200.")
    else:
        print(f"Failure Expected status code 200, but got {response.status_code}.")



def test_api_relations():
    # Define the URL of the API endpoint
    url = "http://bottle:4000/api/relations/fakekey12345"

    # Send a GET request to the endpoint
    response = requests.get(url)

    # Check the status code of the response
    if response.status_code == 404:
        print("Success Status code is 404.")
    else:
        print(f"Failure Expected status code 404, but got {response.status_code}.")

def test_api_crimes_server():
    # Define the URL of the API endpoint
    url = "http://bottle:4000/api/crimes-server"

    # Send a GET request to the endpoint
    response = requests.get(url)

    # Check the status code of the response
    if response.status_code == 200:
        print("Success Status code is 200.")
    else:
        print(f"Failure Expected status code 200, but got {response.status_code}.")


""" def test_api_crime_server():
    # Define the URL of the API endpoint
    url = "http://bottle:4000/api/crimes-server"

    # Send a GET request to the endpoint
    response = requests.get(url)

    # Check the status code of the response
    if response.status_code == 200:
        print("Success Status code is 200.")
    else:
        print(f"Failure Expected status code 200, but got {response.status_code}.")
 """