#!/usr/bin/env python
"""Bottle's command-line utility for administrative tasks."""
import os
import sys
 
 
def test():
    """Run administrative tasks."""
    # Sæt stien til din Bottle-applikation
    sys.path.insert(0, os.path.abspath(os.path.join(os.path.dirname(__file__), '..')))
    os.environ.setdefault('BOTTLE_SETTINGS_MODULE', 'app')
 
    try:
        # Importer din Bottle-applikation
        import app
    except ImportError as exc:
        # Hvis app.py ikke kan importeres, skal du afbryde og vise en fejlbesked
        raise ImportError(
            "Couldn't import the Bottle application. Are you sure it's installed and "
            "available on your PYTHONPATH environment variable? Did you "
            "forget to activate a virtual environment?"
        ) from exc
 
    # Kør Bottle-applikationens hovedfunktion
    app.test(sys.argv)
 
 
if __name__ == '__main__':
    test()